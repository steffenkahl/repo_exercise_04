﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Subtraction : MonoBehaviour
    {
        private int numberOne;
        private int numberTwo;
        private float sum;

        public int NumberOne
        {
            get => numberOne;
            set => numberOne = value;
        }
        
        public int NumberTwo
        {
            get => numberTwo;
            set => numberTwo = value;
        }
        
        public float Sum
        {
            get => sum;
            set => sum = value;
        }

        private void Start()
        {
            Depression();
            Traurig(NumberOne, NumberTwo);
            sum = Absturz(NumberOne, NumberTwo);
            Debug.Log(sum);
        }

        private void Depression()
        {
            sum = numberOne - numberTwo;
        }

        private void Traurig(int numberOne, int numberTwo)
        {
            sum = numberOne - numberTwo;
            Debug.Log(sum);
        }

        private float Absturz(int numberOne, int numberTwo)
        {
            float flauschigkeitswert = 0f;
            flauschigkeitswert = numberOne - numberTwo;
            return flauschigkeitswert;
        }
    }
}

