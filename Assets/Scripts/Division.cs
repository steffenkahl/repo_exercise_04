﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Division : MonoBehaviour
    {
        private int numberOne;
        private int numberTwo;
        private float sum;

        public int NumberOne
        {
            get => numberOne;
            set => numberOne = value;
        }
        public int NumberTwo
        {
            get => numberTwo;
            set => numberTwo = value;
        }
        public float Sum
        {
            get => sum;
            set => sum = value;
        }

        private void Start()
        {
            Teilung();
            Trennung(NumberOne, NumberTwo);
            Sum = Abschied(NumberOne, NumberTwo);
            Debug.Log(Sum);
        }

        private void Teilung()
        {
            sum = numberOne / numberTwo;
        }

        private void Trennung(int numberOne, int numberTwo)
        {
            Sum = numberOne / numberTwo;
            Debug.Log(Sum);
        }

        private float Abschied(int numberOne, int numberTwo)
        {
            float flauschigkeitswert = 0f;
            flauschigkeitswert = numberOne / numberTwo;
            return flauschigkeitswert;
        }
    }
}

