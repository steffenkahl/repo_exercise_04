﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Addition : MonoBehaviour
    {
        private int numberOne;
        private int numberTwo;
        private float sum;

        public int NumberOne
        {
            get => numberOne;
            set => numberOne = value;
        }
        
        public int NumberTwo
        {
            get => numberTwo;
            set => numberTwo = value;
        }
        
        public float Sum
        {
            get => sum;
            set => sum = value;
        }

        private void Start()
        {
            Flauschig();
            Fluffig(NumberOne, NumberTwo);
            sum = Knuddelig(NumberOne, NumberTwo);
            Debug.Log(sum);
        }

        private void Flauschig()
        {
            sum = numberOne + numberTwo;
        }

        private void Fluffig(int numberOne, int numberTwo)
        {
            sum = numberOne + numberTwo;
            Debug.Log(sum);
        }

        private float Knuddelig(int numberOne, int numerTwo)
        {
            float flauschigkeitswert = 0f;
            flauschigkeitswert = numberOne + numerTwo;
            return flauschigkeitswert;
        }
    }
}

